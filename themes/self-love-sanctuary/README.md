
# SELF LOVE SANCTUARY (SLS): GENESIS THEME

https://bitbucket.org/vongoh/self-love-sanctuary

__
#### WP Plugins (install as needed):

#####Marketing Plugins

* Yoast SEO

* Google Analytics Dashboard for WP

* Optin Forms

* Typeform (Contact Page)

* Simple Social Icons

* Social Pug

* Wordpress Ad Widget

* Disqus


<br>

#####Utility Plugins

* Jetpack (for markdown support)

* Duplicator

* UpdraftPlus backup

* Bulletproof Security

* W3 Total Cache

* WP Smush - Image Optimization 

* Send Images to RSS (for MailChimp images)


<br>

#####UI Plugins

* Responsive Menu

* Sexy Author Bio


<br>

#####Dev Plugins

* Genesis Extender

* Widget Logic 

* Genesis Visual Hook Guide

* [Clean Markup Widget](https://perishablepress.com/clean-markup-widget/)