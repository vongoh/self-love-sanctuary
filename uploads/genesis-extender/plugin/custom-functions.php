<?php
/* Do not remove this line. Add your functions below. */

// Remove header right widget area
remove_action( 'load-themes.php', 'genesis_remove_default_widgets_from_header_right' );


remove_action( 'genesis_after_header','genesis_do_nav' ) ;
add_action( 'genesis_header_right','genesis_do_nav' );


//* Add featured image on single post
add_action( 'genesis_entry_header', 'single_post_featured_image', 15 );

function single_post_featured_image() {

if ( ! is_singular( 'post' ) )
return;

$img = genesis_get_image( array( 'format' => 'html', 'size' => genesis_get_option( 'image_size' ), 'attr' => array( 'class' => 'post-image' ) ) );
printf( '<a href="%s" title="%s">%s</a>', get_permalink(), the_title_attribute( 'echo=0' ), $img );

}




