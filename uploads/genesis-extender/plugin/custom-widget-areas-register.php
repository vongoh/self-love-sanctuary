<?php
/**
 * Register Custom Widget Areas.
 */

genesis_register_sidebar( array(
	'id' 			=>	'sls_stories_intro',
	'name'			=>	__( 'sls_stories_intro', 'extender' ),
	'description' 	=>	__( 'Intro box for SLS Stories Category archive', 'extender' )
) );
