<?php
/**
 * Build and Hook-In Custom Widget Areas.
 */

/* Name: sls_stories_intro */

add_action( 'genesis_before_content', 'genesis_extender_sls_stories_intro_widget_area', 10 );
function genesis_extender_sls_stories_intro_widget_area() {
	genesis_extender_sls_stories_intro_widget_area_content();
}

function genesis_extender_sls_stories_intro_widget_area_content() {
	if ( is_category(sls-stories) ) {
		genesis_widget_area( 'sls_stories_intro', $args = array (
			'before'              => '<div id="sls_stories_intro" class="widget-area genesis-extender-widget-area sls-stories-outer">',
			'after'               => '</div>',
			'before_sidebar_hook' => 'genesis_before_sls_stories_intro_widget_area',
			'after_sidebar_hook'  => 'genesis_after_sls_stories_intro_widget_area'
		) );
	} else {
		return false;
	}
}
