<?php
/**
 * Build and Hook-In Custom Hook Boxes.
 */

/* Name: top_bar */

add_action( 'genesis_before_header', 'genesis_extender_top_bar_hook_box', 10 );
function genesis_extender_top_bar_hook_box() {
	genesis_extender_top_bar_hook_box_content();
}

function genesis_extender_top_bar_hook_box_content() { ?>
<div class="top-bar"></div>
<?php
}

/* Name: optin_form */

add_action( 'genesis_before_content_sidebar_wrap', 'genesis_extender_optin_form_hook_box', 10 );
function genesis_extender_optin_form_hook_box() {
	genesis_extender_optin_form_hook_box_content();
}

function genesis_extender_optin_form_hook_box_content() {
	if ( ! is_page() || is_front_page() ) { ?>
<div class="optin-form-outer">
	<?php echo do_shortcode( '[optinform]' ); ?>
</div>
	<?php } else {
		return false;
	}
}
